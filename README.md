# Mkdocs - Identity Theme

[![MIT License][mit-badge]](LICENSE)
[![PyPI][pypi-badge]][pypi-ref]

## Installation

First, install the package via PyPI:

```sh
pip install mkdocs-identity-theme
```

Then include the theme in your `mkdocs.yml` file:

```yaml
theme:
  name: identity
```

[mit-badge]: https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square
[pypi-badge]: https://img.shields.io/pypi/v/mkdocs-identity-theme.svg?style=flat-square
[pypi-ref]: https://pypi.python.org/pypi/mkdocs-identity-theme

## Screenshot

<a href="https://gitlab.com/lramage/mkdocs-identity-theme"><img src="img/screenshot.png" alt="Identity by HTML5 UP for Mkdocs"></a>
