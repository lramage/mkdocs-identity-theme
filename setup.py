from setuptools import setup, find_packages

VERSION = '0.0.1'

setup(
    name="mkdocs-identity-theme",
    version=VERSION,
    url='https://gitlab.com/lramage/mkdocs-identity-theme',
    license='MIT',
    description='Identity by HTML5 UP for Mkdocs',
    author='Lucas Ramage',
    author_email='ramage.lucas@protonmail.com',
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'mkdocs.themes': [
            'identity = mkdocs_identity',
        ]
    },
    zip_safe=False
)
