# Identity by HTML5 UP
<https://html5up.net>

> Just a fun little profile/card-style template I whipped up during a break between major
> projects. Minimal, responsive, and powered by Responsive Tools + Sass. Enjoy :)
> --[AJ \<aj@lkn.io\>][twitter]

## License

Free for personal and commercial use under the [CC-BY-3.0 License](LICENSE).

## Credits

Demo images courtesy of [Unsplash][unsplash], a radtastic collection of CC0 (public domain) images you can use for pretty much whatever.

Icons by [Font Awesome][fa].

Created with [Responsive Tools][responsive-tools].

[fa]: https://fontawesome.com
[responsive-tools]: https://github.com/ajlkn/responsive-tools
[twitter]: https://twitter.com/ajlkn
[unsplash]: https://unsplash.com
